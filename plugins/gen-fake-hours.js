import Vue from 'vue'

function genFakeOpenHours(seed) {
  const openOptions = ['8:00am', '9:00am', '10:00am', '11:00am', '12:00pm']
  const closeOptions = [
    '2:00pm',
    '3:00pm',
    '4:00pm',
    '5:00pm',
    '6:00pm',
    '7:00pm',
    '8:00pm'
  ]

  return (
    openOptions[seed % openOptions.length] +
    ' - ' +
    closeOptions[seed % closeOptions.length]
  )
}

Vue.prototype.$genFakeOpenHours = seed => genFakeOpenHours(seed)
