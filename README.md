# kwusen-assessment

> My dazzling Nuxt.js project

For either option below, access the app with http://localhost:3000/
*Using any hostname other than 'localhost' will violate the currently configured same-origin policy for fetching the data.*

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

## Or simply use docker-compose
``` bash
$ docker-compose up
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
